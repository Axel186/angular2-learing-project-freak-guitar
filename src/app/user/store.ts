import {IUser, USER_INIT} from './model';
import {
  LOGIN, login, logout, LOGOUT_USER, update, UPDATE_USER, UPDATE_USER_FAIL, INIT_USER, init,
  INIT_USER_FAIL, init_user_fail
} from './actions';

export function userReducer(state: IUser = USER_INIT, action): IUser {
  switch (action.type) {
    case LOGIN:
      return login(state, action);
    // case REGISTER:
    //   return register(state, action);
    case UPDATE_USER_FAIL:
      alert('ERROR!'); // TODO: FInd a way to back to previous state.
      return state;
    case UPDATE_USER:
      return update(state, action);
    case LOGOUT_USER:
      return logout(state);
    case INIT_USER:
      return init(state, action);
    case INIT_USER_FAIL:
      return init_user_fail(state, action);
    default:
      return state;
  }
}
