import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService, ISignupUser} from '../../../services/auth.service';
import {FormAlert} from '../../../models/FormAlert';
import {config} from '../../../../config';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit, OnDestroy {
  alive = true;

  registrationForm: FormGroup;
  formAlert: FormAlert;

  recaptcha: any;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.formAlert = new FormAlert();
    this.recaptcha = window['grecaptcha'];

    this.registrationForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      name: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.min(6)])],
      repassword: ['', Validators.required]
    });

    this.formAlert.onLoadingChanged.takeWhile(() => this.alive).subscribe(status => {
      if (status) {
        this.registrationForm.disable();
      } else {
        this.registrationForm.enable();
      }
    });
  }

  ngOnInit() {
    const el = document.getElementById('recaptcha');
    this.recaptcha.opt_widget_id = this.recaptcha.render(el, {
      'sitekey': config.recaptcha.sitekey
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit(): void {
    if (this.registrationForm.valid) {
      if (!this.recaptcha.getResponse(this.recaptcha.opt_widget_id)) {
        this.formAlert.danger('Oops.. Please check the captcha.');
        return;
      }

      if (this.registrationForm.get('password').value !== this.registrationForm.get('repassword').value) {
        this.formAlert.danger('Passwords are not the same.');
        return;
      }

      this.formAlert.setLoading(true);

      const properties: ISignupUser = {
        username: this.registrationForm.get('username').value,
        password: this.registrationForm.get('password').value,
        name: this.registrationForm.get('name').value
      };

      this.authService.register(properties)
        .then((res) => {
          this.formAlert.success('You have successfully registered, please login!');
          this.registrationForm.reset();
          this.recaptcha.reset(this.recaptcha.opt_widget_id);
        })
        .catch(err => {
          this.formAlert.danger(err.message);
        });
    } else {
      this.formAlert.danger('Something wrong, please check the form.');
    }
  }
}
