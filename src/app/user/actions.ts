import {tassign} from 'tassign';
import {IUser, USER_INIT} from './model';

export const LOGIN = 'login';
// export const REGISTER = 'register';
export const LOG_OUT = 'logout';

export const LOGOUT_USER = 'logout_user';
export const INIT_USER = 'init_user';
export const INIT_USER_FAIL = 'init_user_fail';
export const UPDATE_USER = 'update_user';
export const UPDATE_USER_REQUEST = 'update_user_request';
export const UPDATE_USER_FAIL = 'update_user_fail';

export function login(state, action) {
  return tassign(state, {username: action.username});
}

// export function register(state, action) {
//   return tassign(state, {});
// }

export function logout(state) {
  return tassign(USER_INIT);
}

export function init(state, action: { profile: IUser }): IUser {
  return tassign(state, action.profile);
}

export function init_user_fail(state, action) {
  return USER_INIT;
}

export function update(state, action): IUser {
  return tassign(state, action.profile);
}

