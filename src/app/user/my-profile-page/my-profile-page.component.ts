import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {NgRedux, select} from 'ng2-redux';
import {IAppState} from '../../store';
import {IUser} from '../model';
import {Subject} from 'rxjs/Subject';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-my-profile-page',
  templateUrl: './my-profile-page.component.html',
  styleUrls: ['./my-profile-page.component.sass']
})
export class MyProfilePageComponent implements OnInit, OnDestroy {
  alive = true;

  @select(state => state.user) user;
  updateProfileRef;

  constructor(private store: NgRedux<IAppState>, private usersService: UsersService) {
    const self = this;
    this.updateProfileRef = function (user) {
      return self.updateProfile(user);
    };
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  updateProfile(user) {
    return this.usersService.update(user);
  }
}
