import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService, ILoginUser} from '../../../services/auth.service';
import {FormAlert} from '../../../models/FormAlert';
import {Router} from '@angular/router';
import {NgRedux} from 'ng2-redux';
import {IAppState} from '../../../store';
import {LOGIN} from '../../actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy {
  alive = true;
  loginForm: FormGroup;
  formAlert: FormAlert;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private ngRedux: NgRedux<IAppState>) {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.formAlert = new FormAlert();
    this.formAlert.onLoadingChanged.takeWhile(() => this.alive).subscribe(status => {
      if (status) {
        this.loginForm.disable();
      } else {
        this.loginForm.enable();
      }
    });
  }

  ngOnInit() {
    document.getElementById('inputEmail').focus();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.formAlert.setLoading(true);

      const properties: ILoginUser = {
        username: this.loginForm.get('username').value,
        password: this.loginForm.get('password').value
      };

      this.authService.login(properties)
        .then(res => {
          this.router.navigate(['/']);
        })
        .catch(err => this.formAlert.danger(err.message));
    } else {
      this.formAlert.danger('Something wrong, please check the form.');
    }
  }
}
