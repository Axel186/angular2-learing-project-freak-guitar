export const USER_ROLE_ADMIN = 3;
export const USER_ROLE_MANAGER = 2;
export const USER_ROLE_REGULAR = 1;

export interface IUser {
  id: string;
  username: string;
  name: string;
  role: number;
  disabled: boolean;
  billingAddress?: string;
  shippingAddress?: string;
}

export const USER_INIT: IUser = {
  id: null,
  username: null,
  name: null,
  role: USER_ROLE_REGULAR,
  disabled: false,
  billingAddress: null,
  shippingAddress: null
};
