import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IProduct} from '../../models/Product';
import {ICartItem} from '../../cart/model';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.sass']
})
export class CartItemComponent implements OnInit {

  @Input() product: any;
  @Input() itemsInCart: Array<ICartItem> = [];
  @Output() onRemoveFromCart = new EventEmitter();

  @Input() isCartPage: boolean = false;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
  }

  getAmount(product: any): number {
    const itemCart = product.amount || this.itemsInCart.find(item => item.productId === product.id);
    return (itemCart) ? itemCart.amount : 0;
  }

  updateAmount(product: any, value) {
    if (!this.itemsInCart.length) {
      product.amount = value;
      return;
    }

    const item = this.itemsInCart.find(item => item.productId === product.id);
    item.amount = Number.parseInt(value);
  }

  removeFromCart(product: IProduct) {
    this.onRemoveFromCart.next(product);
  }
}
