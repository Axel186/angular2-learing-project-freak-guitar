import {Component, OnInit} from '@angular/core';
import {categories} from '../../../data/categories';
import {ICategory} from '../../../models/Categories';

@Component({
  selector: 'app-categories-menu',
  templateUrl: './categories-menu.component.html',
  styleUrls: ['./categories-menu.component.sass']
})
export class CategoriesMenuComponent implements OnInit {
  mainCategories: Array<ICategory>;
  childrenCategories: Array<ICategory>;

  constructor() {
    this.mainCategories = categories.filter((category) => (category.parentId === null));
    this.childrenCategories = categories.filter((category) => (category.parentId !== null));
  }

  ngOnInit() {
  }

  getChildrenOfCategory(category: ICategory): Array<ICategory> {
    return this.childrenCategories.filter((child) => (child.parentId === category.id));
  }
}
