import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {NgRedux, select} from 'ng2-redux';
import {IAppState} from '../../../store';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.sass']
})
export class UserMenuComponent implements OnInit, OnDestroy {
  alive = true;
  statusAuth: boolean; // TODO: Maybe it's better to point on the service's `statusAuth` instead of managing another one.

  itemsInCart: number;

  constructor(private authService: AuthService, private router: Router, private store: NgRedux<IAppState>) {
    this.itemsInCart = 0;
    this.store.select(state => state.cart.items).takeWhile(() => this.alive).subscribe((items) => {
      this.itemsInCart = items.length;
    });

    this.statusAuth = this.authService.isAuthenticated();
    this.authService.onAuthChanged.takeWhile(() => this.alive).subscribe((status: boolean) => this.statusAuth = status);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  logOut() {
    this.authService.logout()
      .then(() => this.router.navigate(['/']));
  }
}
