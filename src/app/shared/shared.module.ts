import {NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {CommonModule} from '@angular/common';
import {UserMenuComponent} from './header/user-menu/user-menu.component';
import {CategoriesMenuComponent} from './header/categories-menu/categories-menu.component';
import {RouterModule} from '@angular/router';
import {NgReduxModule} from 'ng2-redux';
import {ProfileComponent} from './profile/profile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CartItemComponent } from './cart-item/cart-item.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    UserMenuComponent,
    CategoriesMenuComponent,
    ProfileComponent,
    CartItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgReduxModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ProfileComponent,
    CartItemComponent
  ]
})
export class SharedModule {
}
