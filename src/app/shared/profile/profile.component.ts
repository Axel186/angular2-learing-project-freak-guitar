import {Component, OnDestroy, OnInit, Input, OnChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgRedux} from 'ng2-redux';
import {IAppState} from '../../store';
import {UsersService} from '../../services/users.service';
import {tassign} from 'tassign';
import {FormAlert} from '../../models/FormAlert';
import {Router} from '@angular/router';
import {IUser} from '../../user/model';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit, OnDestroy, OnChanges {
  alive = true;

  formAlert: FormAlert;
  myProfileForm: FormGroup;

  @Input() user: IUser | null;
  @Input() profileAction;
  @Input() isMine: boolean;

  isAdministrator: boolean;
  email: string;

  constructor(private fb: FormBuilder, private store: NgRedux<IAppState>, private usersService: UsersService, private router: Router, private authService: AuthService) {
    this.formAlert = new FormAlert();
    this.authService.isAdministrator.takeWhile(() => this.alive).subscribe((isAdministrator: boolean) => this.isAdministrator = isAdministrator);

    this.myProfileForm = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z\\s]*')])],
      billingAddress: [''],
      shippingAddress: [''],
      role: ['']
    });

    this.formAlert.onLoadingChanged.takeWhile(() => this.alive).subscribe(status => {
      if (status) {
        this.myProfileForm.disable();
      } else {
        this.myProfileForm.enable();
      }
    });
  }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.user) {
      this.myProfileForm.patchValue({
        name: this.user.name,
        billingAddress: this.user.billingAddress,
        shippingAddress: this.user.shippingAddress,
        role: this.user.role
      });

      this.email = this.user.username;
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {
    if (this.myProfileForm.valid) {
      this.formAlert.setLoading(true);

      const user = tassign(this.user, this.myProfileForm.value);

      this.profileAction(user)
        .then(res => {
          this.formAlert.success('Profile have updated!');
        })
        .catch(err => {
          this.formAlert.danger(err.message);
        });
    } else {
      this.formAlert.danger('Something wrong, please check the form.');
    }
  }

  onDelete() {
    if (!confirm('Are you sure?')) {
      return;
    }

    this.usersService.delete(this.user).then(() => this.router.navigate(['/users']));
  }
}
