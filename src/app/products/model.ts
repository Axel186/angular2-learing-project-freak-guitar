// export interface IProduct {
//   id: string;
//   name: string;
//   price: number;
//   categoryId: string;
//   description: string;
// }

import {IProduct} from '../models/Product';

export const PRODUCT_INITIAL_STATE: IProduct = {
  id: '',
  name: '',
  price: null,
  categoryId: '',
  description: '',
  image: ''
};

export interface IProductsState {
  products: Array<IProduct>;
  selectedProduct: IProduct;
  action: string;
}

export const PRODUCTS_INITIAL_STATE: IProductsState = {
  products: [],
  selectedProduct: PRODUCT_INITIAL_STATE,
  action: 'initialize'
};
