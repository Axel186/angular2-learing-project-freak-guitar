import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {categories} from '../../data/categories';
import {ICategory} from '../../models/Categories';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.sass']
})
export class CategoryPageComponent implements OnInit, OnDestroy {
  alive = true;
  category: ICategory;

  constructor(private route: ActivatedRoute, private authService: AuthService) {
    this.route.params.takeWhile(() => this.alive).subscribe(params => {
      this.category = categories.find(category => category.id === params.id);
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
