import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {ProductsService} from '../../../services/products.service';
import {NgRedux} from 'ng2-redux';
import {IAppState, rootReducer} from '../../../store';
import {getProducts} from '../../store';
import {ICategory} from '../../../models/Categories';
import {CartService} from '../../../services/cart.service';
import {IProduct} from '../../../models/Product';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.sass']
})
export class CategoryComponent implements OnChanges, OnDestroy, OnInit {
  alive = true;

  @Input() category: ICategory;
  products: Array<any>;

  constructor(private productsService: ProductsService, private store: NgRedux<IAppState>, private cartService: CartService, private authService: AuthService) {
  }

  ngOnInit() {
    this.store.select(state => state.products.products.filter(item => item.categoryId === this.category.id))
      .takeWhile(() => this.alive)
      .subscribe(res => {
        this.products = res;
      });
  }

  ngOnChanges() {
    this.productsService.fetch(this.category.id);
  }

  ngOnDestroy() {
    this.alive = false;
  }

  addToCart(product: IProduct) {
    this.cartService.add(product, 1);
  }
}
