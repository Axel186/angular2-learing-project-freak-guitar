import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EditProductPageComponent} from './edit-product-page/edit-product-page.component';
import {ManageProductsGuardService} from './manageProductsGuard.service';

const rootPath = 'manage-product/';
// const rootPath = '';

const routes: Routes = [
  {
    path: rootPath + 'category/new-product/:cat_id',
    component: EditProductPageComponent,
    canActivate: [ManageProductsGuardService]
  },
  {
    path: rootPath + 'product/:id/edit',
    component: EditProductPageComponent,
    canActivate: [ManageProductsGuardService]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class ManageProductsRoutingModule {
}
