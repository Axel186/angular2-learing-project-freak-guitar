import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditProductComponent} from './edit-product-page/edit-product/edit-product.component';
import {ManageProductsRoutingModule} from './manageProducts-routing.module';
import {EditProductPageComponent} from './edit-product-page/edit-product-page.component';
import {SharedModule} from '../../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    EditProductComponent,
    EditProductPageComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ManageProductsRoutingModule,
    SharedModule
  ]
})
export class ManageProductsModule {
}
