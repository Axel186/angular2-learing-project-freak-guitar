import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IProduct} from '../../../../models/Product';
import {ICategory} from '../../../../models/Categories';
import {ProductsService} from '../../../../services/products.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {PRODUCT_INITIAL_STATE, PRODUCTS_INITIAL_STATE} from '../../../model';
import {AuthService} from '../../../../services/auth.service';
import {categories} from '../../../../data/categories';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.sass']
})
export class EditProductComponent implements OnInit, OnChanges {
  // @Input() onProduct: Observable<IProduct>;
  @Input() category: ICategory;

  productForm: FormGroup;
  @Input() product: IProduct;

  constructor(private fb: FormBuilder, private productsService: ProductsService, private router: Router, private authService: AuthService) {
    this.product = PRODUCT_INITIAL_STATE;

    this.productForm = this.fb.group({
      name: ['', [Validators.required]],
      image: ['', [Validators.required]],
      description: ['', [Validators.required]],
      price: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]+'), Validators.min(0)])],
    });
  }

  ngOnInit() {
    this.productForm.patchValue(this.product);
  }

  ngOnChanges() {
    if (!this.category && this.product && this.product.categoryId) {
      this.category = categories.find(item => item.id === this.product.categoryId);
    }
  }

  onSubmit(): void {
    const properties = <IProduct>{
      name: this.productForm.get('name').value,
      image: this.productForm.get('image').value,
      description: this.productForm.get('description').value,
      price: this.productForm.get('price').value,
      categoryId: (this.category) ? this.category.id : this.product.categoryId,
      id: (this.product) ? this.product.id : null
    };

    if (!this.product.id) {
      this.productsService.create(properties)
        .then(() => {
          const cat_id = this.category.id || this.product.categoryId;
          this.router.navigate(['/category', cat_id]);
        })
        .catch((err) => {
          // TODO: Show error
          alert(err);
        });
    } else {
      this.productsService.update(properties)
        .then(() => {
          alert('Saved!');
        })
        .catch((err) => {
          // TODO: Show error
          alert(err);
        });
    }
  }

  onDelete(): void {
    if (!confirm('Are you sure?')) {
      return;
    }

    const categoryId = this.product.categoryId;
    this.productsService.delete(this.product)
      .then(() => {
        this.router.navigate(['/category', categoryId]);
      })
      .catch((err) => {
        // TODO: Show error
        alert(err);
      });
  }
}
