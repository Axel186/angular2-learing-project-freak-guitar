import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IProduct} from '../../../models/Product';
import {ICategory} from '../../../models/Categories';
import {categories} from '../../../data/categories';
import {ProductsService} from '../../../services/products.service';
import {NgRedux, select} from 'ng2-redux';
import {IAppState} from '../../../store';
import {SELECT_PRODUCT} from '../../actions';
import {PRODUCT_INITIAL_STATE} from '../../model';

@Component({
  selector: 'app-edit-product-page',
  templateUrl: './edit-product-page.component.html',
  styleUrls: ['./edit-product-page.component.sass']
})
export class EditProductPageComponent implements OnInit, OnDestroy {
  alive = true;

  product: IProduct;
  category: ICategory;

  constructor(private route: ActivatedRoute, private productsService: ProductsService, private store: NgRedux<IAppState>) {
    this.route.params.takeWhile(() => this.alive).subscribe((params) => {
      this.product = PRODUCT_INITIAL_STATE;

      if (params.id) {
        // Load the product request.
        this.productsService.select(params.id);
        this.store.select(state => state.products.selectedProduct).takeWhile(() => this.alive).subscribe(item => {
          this.product = item;
        });
      }

      if (params.cat_id) {
        this.category = categories.find(category => (category.id === params.cat_id));
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
