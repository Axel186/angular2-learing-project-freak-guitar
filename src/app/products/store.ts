import {IProductsState, PRODUCTS_INITIAL_STATE} from './model';
import {
  CREATE_PRODUCT_FAIL, CREATE_PRODUCT_SUCCESS, createProductFail, createProductSuccess,
  FETCH_PRODUCTS, FETCH_PRODUCTS_FAIL, FETCH_PRODUCTS_SUCCESS, fetchProducts, fetchProductsFail, fetchProductsSuccess,
  SAVE_PRODUCT,
  saveProduct, SELECT_PRODUCT, selectProduct, UPDATE_PRODUCT_FAIL, updateProductFail, UPDATE_PRODUCT_SUCCESS,
  updateProductSuccess, DELETE_PRODUCT_FAIL, deleteProductFail, DELETE_PRODUCT_SUCCESS, deleteProductSuccess,
  FETCH_PRODUCT
} from './actions';
import {createSelector} from 'reselect';

export function productsReducer(state: IProductsState = PRODUCTS_INITIAL_STATE, action): IProductsState {
  switch (action.type) {
    case SAVE_PRODUCT:
      return saveProduct(state, action);
    case FETCH_PRODUCTS:
      return fetchProducts(state, action);
    case FETCH_PRODUCTS_FAIL:
      return fetchProductsFail(state, action);
    case FETCH_PRODUCTS_SUCCESS:
      return fetchProductsSuccess(state, action);
    case CREATE_PRODUCT_FAIL:
      return createProductFail(state, action);
    case CREATE_PRODUCT_SUCCESS:
      return createProductSuccess(state, action);
    case UPDATE_PRODUCT_FAIL:
      return updateProductFail(state, action);
    case UPDATE_PRODUCT_SUCCESS:
      return updateProductSuccess(state, action);
    case DELETE_PRODUCT_FAIL:
      return deleteProductFail(state, action);
    case DELETE_PRODUCT_SUCCESS:
      return deleteProductSuccess(state, action);
    case SELECT_PRODUCT:
      return selectProduct(state, action);
    default:
      return state;
  }
}

const productsSelector = state => state.products;
export const getProducts = createSelector(
  productsSelector,
  (res) => res.products
);

const productSelector = (state, id) => {
  return state.products.find(item => item.id === id);
};
