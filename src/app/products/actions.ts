import {tassign} from 'tassign';

export const SAVE_PRODUCT = 'save_product';

export const FETCH_PRODUCTS = 'fetch_products';
export const FETCH_PRODUCTS_FAIL = 'fetch_products_fail';
export const FETCH_PRODUCTS_SUCCESS = 'fetch_products_success';
export const FETCH_PRODUCT = 'fetch_product';

export const CREATE_PRODUCT_FAIL = 'create_product_fail';
export const CREATE_PRODUCT_SUCCESS = 'create_product_success';

export const UPDATE_PRODUCT_FAIL = 'update_product_fail';
export const UPDATE_PRODUCT_SUCCESS = 'update_product_success';

export const DELETE_PRODUCT_FAIL = 'delete_product_fail';
export const DELETE_PRODUCT_SUCCESS = 'delete_product_success';

export const SELECT_PRODUCT = 'select_product';

const failProductState = function (state, action) {
  return tassign(state, {action: action.action});
};

export function saveProduct(state, action) {
  return tassign(state, action);
}

export function fetchProducts(state, action) {
  return tassign(state, {action: action.action});
}

export const fetchProductsFail = failProductState;

export function fetchProductsSuccess(state, action) {
  const products = state.products.slice(0); // Clone

  Object.keys(action.products).forEach(id => {
    const product = tassign(action.products[id], {id: id});

    const found = products.find(item => item.id === product.id);
    if (found) {
      const index = state.products.map(item => item.id).indexOf(product.id);
      products[index] = tassign(products[index], product);
    } else {
      products.push(product);
    }
  });

  if (state.action === 'fetch products') {
    return tassign(state, {products: products, action: action.action});
  } else {
    return state;
  }
}

export const createProductFail = failProductState;

export function createProductSuccess(state, action) {
  return tassign(state, {products: state.products.concat(action.product), action: action.action});
}

export const updateProductFail = failProductState;

export function updateProductSuccess(state, action) {
  const product = state.products.find(item => item.id === action.product.id);
  const index = state.products.map(item => item.id).indexOf(action.product.id);

  return tassign(state, {
    products: [
      ...state.products.slice(0, index),
      tassign(product, action.product),
      ...state.products.slice(index + 1)
    ],
    action: action.action,
    selectedProduct: action.product
  });
}

export const deleteProductFail = failProductState;

export function deleteProductSuccess(state, action) {
  return tassign(state, {
    products: state.products.filter(item => item.id !== action.id)
  });
}

export function selectProduct(state, action) {
  return tassign(state, {action: 'select product', selectedProduct: action.product});
}

// export function loadProducts()
