import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-preview-small',
  templateUrl: './item-preview-small.component.html',
  styleUrls: ['./item-preview-small.component.sass']
})
export class ItemPreviewSmallComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
