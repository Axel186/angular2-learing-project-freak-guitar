import {Subject} from 'rxjs/Subject';

export interface IFormAlert {
  status: boolean;
  message: string;
  loading: boolean;
}

const FORM_ALERT_INIT = {
  status: false,
  message: '',
  loading: false
};

export class FormAlert {
  status;
  message;
  loading;

  onLoadingChanged = new Subject<boolean>();

  constructor(properties: IFormAlert = FORM_ALERT_INIT) {
    this.status = properties.status || false;
    this.message = properties.message || '';
    this.loading = properties.loading || false;
  }

  danger(errorMessage: string) {
    this.status = false;
    this.message = errorMessage;

    this.setLoading(false);
  }

  success(successMessage: string) {
    this.status = true;
    this.message = successMessage;

    this.setLoading(false);
  }

  getClassName() {
    return (this.status) ? 'alert-success' : 'alert-danger';
  }

  setLoading(status: boolean) {
    if (this.loading === status) {
      return;
    }

    this.loading = status;
    this.onLoadingChanged.next(this.loading);
  }
}
