// RSJX
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/toPromise';

// Angular
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {HomePageComponent} from './home-page/home-page.component';
import {NewestItemsComponent} from './items/newest-items/newest-items.component';
import {BannerComponent} from './home-page/banner/banner.component';
import {ItemPreviewSmallComponent} from './items/item-preview-small/item-preview-small.component';
import {LoginComponent} from './user/login-page/login/login.component';
import {LoginPageComponent} from './user/login-page/login-page.component';
import {RegistrationPageComponent} from './user/registration-page/registration-page.component';
import {RegistrationComponent} from './user/registration-page/registration/registration.component';
import {NgRedux, NgReduxModule, DevToolsExtension} from 'ng2-redux';
import {IAppState, INITIAL_STATE, rootReducer} from './store';
import {AuthService} from './services/auth.service';
import {MyProfilePageComponent} from './user/my-profile-page/my-profile-page.component';
import {CategoryPageComponent} from './products/category-page/category-page.component';
import {CategoryComponent} from './products/category-page/category/category.component';
import {ProductsService} from './services/products.service';
import {ManageProductsModule} from './products/manageProducts/manageProducts.module';
import {SharedModule} from './shared/shared.module';
import {CartService} from './services/cart.service';
import {CartPageComponent} from './cart/cart-page/cart-page.component';
import {CartComponent} from './cart/cart-page/cart/cart.component';
import {OrdersPageComponent} from './orders/orders-page/orders-page.component';
import {OrdersComponent} from './orders/orders-page/orders/orders.component';
import {OrdersService} from './services/orders.service';
import {ManageUsersModule} from './users/manage-users.module';
import {UsersService} from './services/users.service';
import {OrLineComponent} from './user/or-line/or-line.component';
import {AppRoutingModule} from './app-routing.module';
import {ManageProductsGuardService} from './products/manageProducts/manageProductsGuard.service';
import {ManageUsersGuardService} from './users/manageUsersGuard.service';
import {AppRouteGuardService} from './appRouteGuard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NewestItemsComponent,
    BannerComponent,
    ItemPreviewSmallComponent,
    LoginComponent,
    LoginPageComponent,
    RegistrationPageComponent,
    RegistrationComponent,
    MyProfilePageComponent,
    CategoryPageComponent,
    CategoryComponent,
    CartPageComponent,
    CartComponent,
    OrdersPageComponent,
    OrdersComponent,
    OrLineComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    NgReduxModule,
    SharedModule,
    ManageProductsModule,
    ManageUsersModule
  ],
  providers: [
    AuthService,
    ProductsService,
    CartService,
    OrdersService,
    UsersService,
    ManageProductsGuardService,
    ManageUsersGuardService,
    AppRouteGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], [devTools.enhancer()]);
  }
}
