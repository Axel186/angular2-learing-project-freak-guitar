

import {ICategory} from '../models/Categories';

export const categories: Array<ICategory> = [

  /////////////////////////////////////////
  // Main Categories
  /////////////////////////////////////////
  {
    id: '89fe5050-4ef8-4b3a-a246-84e01371e284',
    parentId: null,
    name: 'Guitars'
  }, {
    id: '8b31e768-86b9-4ac8-8330-b4b9f95ff37a',
    parentId: null,
    name: 'Amps'
  }, {
    id: 'fd8ab4bf-85b4-4e05-9fb3-3f048efcb5bf',
    parentId: null,
    name: 'Effects'
  }, {
    id: 'bdacff90-e78b-4e31-9e51-9aec8a868ea5',
    parentId: null,
    name: 'Studio'
  },

  /////////////////////////////////////////
  // Children
  /////////////////////////////////////////

  // Guitars
  {
    id: 'ff265324-4f6a-4e8c-a69d-0e3f4015f61b',
    parentId: '89fe5050-4ef8-4b3a-a246-84e01371e284',
    name: 'Electric Guitars'
  }, {
    id: '7d076350-74a9-4588-b9f4-e06c65106e62',
    parentId: '89fe5050-4ef8-4b3a-a246-84e01371e284',
    name: 'Bass Guitars'
  },

  // Amps
  {
    id: 'f4eaa011-ac26-4efe-87bf-24d313eff131',
    parentId: '8b31e768-86b9-4ac8-8330-b4b9f95ff37a',
    name: 'Guitar Amplifier'
  }, {
    id: '236ee712-4be9-4c09-85f3-b1201fb65b07',
    parentId: '8b31e768-86b9-4ac8-8330-b4b9f95ff37a',
    name: 'Bass Amplifier'
  },

  // Effects
  {
    id: '2e956b88-a1f2-4ac9-946c-476bbd3a69c5',
    parentId: 'fd8ab4bf-85b4-4e05-9fb3-3f048efcb5bf',
    name: 'Distortion'
  }, {
    id: '2fc35fdd-cca8-46de-b8a6-6f855ad672f0',
    parentId: 'fd8ab4bf-85b4-4e05-9fb3-3f048efcb5bf',
    name: 'Delay'
  }, {
    id: 'c09cc693-a5cd-4980-b28f-74648e59feb8',
    parentId: 'fd8ab4bf-85b4-4e05-9fb3-3f048efcb5bf',
    name: 'EQ'
  }, {
    id: '5e6c24a7-009a-4676-9ca8-825a399ca742',
    parentId: 'fd8ab4bf-85b4-4e05-9fb3-3f048efcb5bf',
    name: 'Modulation'
  },

  // Studio
  {
    id: '6c392ef0-b6ce-49ad-93be-f05ed22d89f2',
    parentId: 'bdacff90-e78b-4e31-9e51-9aec8a868ea5',
    name: 'Sound card'
  }, {
    id: '644ee910-d788-430a-a26d-b84eb3ec7a5b',
    parentId: 'bdacff90-e78b-4e31-9e51-9aec8a868ea5',
    name: 'Speakers'
  }, {
    id: '462e6e31-efe1-4546-8808-b40f7a38c7cf',
    parentId: 'bdacff90-e78b-4e31-9e51-9aec8a868ea5',
    name: 'Microphones'
  }
];
