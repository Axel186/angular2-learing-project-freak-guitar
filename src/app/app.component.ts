import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';
import {config} from '../config';
import {AuthService} from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.setupAuthService();
  }
}
