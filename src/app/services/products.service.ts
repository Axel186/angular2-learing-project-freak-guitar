import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {NgRedux} from 'ng2-redux';
import {IAppState} from '../store';
import {
  CREATE_PRODUCT_FAIL, CREATE_PRODUCT_SUCCESS, DELETE_PRODUCT_FAIL, DELETE_PRODUCT_SUCCESS, FETCH_PRODUCTS,
  FETCH_PRODUCTS_FAIL,
  FETCH_PRODUCTS_SUCCESS, SELECT_PRODUCT, UPDATE_PRODUCT_FAIL, UPDATE_PRODUCT_SUCCESS
} from '../products/actions';
import {IProduct} from '../models/Product';
import {AuthService} from './auth.service';
import {assign} from 'rxjs/util/assign';
import {productsReducer} from '../products/store';
import {IProductsState} from '../products/model';
import {config} from '../../config';
import {Observable} from 'rxjs/Observable';

const SERVER_URL = config.server.protocol + '://' + config.server.domain + '/';

@Injectable()
export class ProductsService {

  constructor(private http: Http, private store: NgRedux<IAppState>, private authService: AuthService) {
  }

  fetch(categoryId: string): Promise<any> {
    this.store.dispatch({type: FETCH_PRODUCTS, action: 'fetch products'});

    return this.http.get(SERVER_URL + 'products.json?orderBy="categoryId"&equalTo="' + categoryId + '"')
      .toPromise()
      .then(response => response.json() as Array<any>)
      .then(items => {
        this.store.dispatch({type: FETCH_PRODUCTS_SUCCESS, products: items, action: 'fetched successfully'});
      })
      .catch(err => {
        this.store.dispatch({type: FETCH_PRODUCTS_FAIL, action: err.error});
      });
  }

  fetchSingle(id: string): Promise<any> {
    this.store.dispatch({type: FETCH_PRODUCTS, action: 'fetch products'});

    return this.http.get(SERVER_URL + 'products/' + id + '.json?')
      .toPromise()
      .then(response => response.json() as IProduct)
      .then(item => {
        const arr = [];
        arr[item.id] = item;
        this.store.dispatch({type: FETCH_PRODUCTS_SUCCESS, products: arr});

        return item;
      })
      .catch(err => {
        this.store.dispatch({type: FETCH_PRODUCTS_FAIL, action: err.error});
      });
  }

  select(id): Promise<any> {
    const selectedProduct = this.store.getState().products.products.find(item => item.id === id);
    if (selectedProduct) {
      this.store.dispatch({type: SELECT_PRODUCT, product: selectedProduct});
    } else {
      return this.fetchSingle(id).then((item) => {
        this.store.dispatch({type: SELECT_PRODUCT, product: item});
      });
    }

    return Promise.resolve();
  }

  create(product: IProduct): Promise<any> {
    const data = product;
    return this.http.post(SERVER_URL + 'products.json?auth=' + this.authService.token, data)
      .toPromise()
      .then(response => response.json())
      .then(res => {
        product.id = res.name;
        this.store.dispatch({type: CREATE_PRODUCT_SUCCESS, product: product, action: 'successfully created'});
      })
      .catch(err => {
        this.store.dispatch({type: CREATE_PRODUCT_FAIL, action: err.error});
      });
  }

  update(product: IProduct): Promise<any> {
    const data = product;

    return this.http.put(SERVER_URL + 'products/' + product.id + '.json?auth=' + this.authService.token, data)
      .toPromise()
      .then(response => response.json())
      .then(res => {
        this.store.dispatch({type: UPDATE_PRODUCT_SUCCESS, product: product, action: 'successfully updated'});
      })
      .catch(err => {
        this.store.dispatch({type: UPDATE_PRODUCT_FAIL, action: err});
      });
  }

  delete(product: IProduct): Promise<any> {
    return this.http.delete(SERVER_URL + 'products/' + product.id + '.json?auth=' + this.authService.token)
      .toPromise()
      .then(() => {
        this.store.dispatch({type: DELETE_PRODUCT_SUCCESS, id: product.id});
      })
      .catch(err => {
        this.store.dispatch({type: DELETE_PRODUCT_FAIL, action: err});
      });
  }

  getProductAttrById(productId: string, key): Observable<string> {
    return this.store.select(state => {
      const product = state.products.products.find(item => item.id === productId);

      if (product) {
        return product[key];
      } else {
        if (state.products.action !== 'fetch products') {
          this.fetchSingle(productId);
        }
      }

      return '';
    });
  }
}
