import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {NgRedux} from 'ng2-redux';
import {AuthService} from './auth.service';
import {IAppState} from '../store';
import {tassign} from 'tassign';
import {DELETE_USER, FETCH_USERS, UPDATE_USERS} from '../users/actions';
import {IUser} from '../user/model';
import {UPDATE_USER, UPDATE_USER_FAIL} from '../user/actions';
import {config} from '../../config';
import {assign} from 'rxjs/util/assign';
import {FETCH_USERS_SUCCESS} from '../users/actions';
import {Observable} from 'rxjs/Observable';
import * as firebase from 'firebase';

const SERVER_URL = config.server.protocol + '://' + config.server.domain + '/';

@Injectable()
export class UsersService {
  constructor(private http: Http, private store: NgRedux<IAppState>, private authService: AuthService) {
  }

  fetchAll(): Promise<any> {
    this.store.dispatch({type: FETCH_USERS});
    return this.http.get(SERVER_URL + '/profiles.json?auth=' + this.authService.token)
      .toPromise()
      .then(response => response.json())
      .then(users => {
        const usersArr = Object.keys(users).map(id => {
          if (users[id].username && !users[id].disabled) {
            return tassign(users[id], {id: id});
          }
        });

        this.store.dispatch({type: FETCH_USERS_SUCCESS, users: users});
        return usersArr;
      });
  }

  delete(user: IUser): firebase.Promise<any> {
    return this.http.patch(SERVER_URL + 'profiles/' + user.id + '.json?auth=' + this.authService.token, {disabled: true})
      .toPromise()
      .then(() => this.store.dispatch({
        type: DELETE_USER,
        userId: user.id
      }));
  }

  update(properties): Promise<any> {
    let profileUpdate = {
      name: properties.name,
      billingAddress: properties.billingAddress,
      shippingAddress: properties.shippingAddress
    };

    if (this.authService.isAdministrator) {
      profileUpdate = assign(profileUpdate, {role: Number.parseInt(properties.role)});
    }

    return this.http.patch(SERVER_URL + 'profiles/' + properties.id + '.json?auth=' + this.authService.token, profileUpdate)
      .toPromise()
      .then(response => response.json())
      .then(res => {
        // this.store.dispatch({type: UPDATE_USER, profile: profileUpdate});
        this.store.dispatch({type: UPDATE_USERS, profile: profileUpdate, id: properties.id});
      })
      .catch(err => this.store.dispatch({type: UPDATE_USER_FAIL}));
  }

  fetch(userId): Promise<any> {
    this.store.dispatch({type: FETCH_USERS});
    return this.http.get(SERVER_URL + 'profiles/' + userId + '.json?auth=' + this.authService.token)
      .toPromise()
      .then(response => response.json())
      .then(user => {
        const arr = [];
        arr[userId] = user;

        if (user && user.username) {
          this.store.dispatch({type: FETCH_USERS_SUCCESS, users: arr});
        } else {
          this.store.dispatch({type: FETCH_USERS_SUCCESS, users: []});
        }
      });
  }

  getUserAttrById(userId: string, key): Observable<string> {
    return this.store.select(state => {
      const user = state.users.users.find(item => item.id === userId);

      if (user) {
        return user[key];
      } else {
        if (state.users.action !== 'fetching') {
          this.fetch(userId);
        }
      }

      return '';
    });
  }

}
