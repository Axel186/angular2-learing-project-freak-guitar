import {Injectable} from '@angular/core';
import {NgRedux} from 'ng2-redux';
import {IAppState} from '../store';
import {IOrder, ORDER_STATUS_NEW} from '../orders/model';
import {CREATE_ORDER, DELETE_ORDER, FETCH_ORDERS, UPDATE_ORDER} from '../orders/actions';
import {Http} from '@angular/http';
import {AuthService} from './auth.service';
import {tassign} from 'tassign';
import {config} from '../../config';
import {assign} from 'rxjs/util/assign';

const SERVER_URL = config.server.protocol + '://' + config.server.domain + '/';

@Injectable()
export class OrdersService {

  constructor(private store: NgRedux<IAppState>, private http: Http, private authService: AuthService) {
  }

  fetch(): Promise<any> {
    let request = '';
    if (!this.authService.isManager.value) {
      request = 'orderBy="userId"&equalTo="' + this.authService.uid + '"&';
    }

    return this.http.get(SERVER_URL + 'orders.json?' + request + 'auth=' + this.authService.token)
      .toPromise()
      .then(response => response.json())
      .then(orders => {
        const ordersArr = Object.keys(orders).map(id => {
          return tassign(orders[id], {id: id});
        });

        this.store.dispatch({type: FETCH_ORDERS, orders: orders});
        return ordersArr;
      });
  }

  create(order: IOrder): Promise<any> {
    const data = {
      items: order.items,
      userId: order.userId,
      status: ORDER_STATUS_NEW
    };

    return this.http.post(SERVER_URL + 'orders.json?auth=' + this.authService.token, data)
      .toPromise()
      .then(response => response.json())
      .then(res => {
          order.id = res.name;
          this.store.dispatch({type: CREATE_ORDER, order: order});
          return order;
        }
      )
      // .catch(err => this.store.dispatch({type: CREATE_ORDER_FAIL, order: order}))
      ;
  }

  update(order: IOrder): Promise<any> {
    const data = {
      items: order.items,
      // userId: order.userId,
      status: order.status
    };

    return this.http.patch(SERVER_URL + 'orders/' + order.id + '.json?auth=' + this.authService.token, data)
      .toPromise()
      .then(response => response.json())
      .then(resOrder => {
        resOrder = tassign(resOrder, {id: order.id});
        this.store.dispatch({type: UPDATE_ORDER, order: resOrder});
        return resOrder;
      });
  }

  delete(order: IOrder): Promise<any> {
    return this.http.delete(SERVER_URL + 'orders/' + order.id + '.json?auth=' + this.authService.token)
      .toPromise()
      .then(() => this.store.dispatch({
        type: DELETE_ORDER,
        orderId: order.id
      }));
  }
}
