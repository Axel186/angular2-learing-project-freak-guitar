import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import {Subject} from 'rxjs/Subject';
import {config} from '../../config';
import {NgRedux, select} from 'ng2-redux';
import {IAppState} from '../store';
import {
  INIT_USER, INIT_USER_FAIL, LOGOUT_USER, UPDATE_USER, UPDATE_USER_FAIL,
} from '../user/actions';
import {Http} from '@angular/http';
import {createProfile, fetchProfile} from './users.helpers';
import {tassign} from 'tassign';
import {IUser, USER_ROLE_ADMIN, USER_ROLE_MANAGER} from '../user/model';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {CLEAR_USERS} from '../users/actions';
import {CLEAR_ORDERS} from "app/orders/actions";

export interface ISignupUser {
  username: string;
  password: string;
  name: string;
}

export interface ILoginUser {
  username: string;
  password: string;
}

@Injectable()
export class AuthService {
  token: string;
  onAuthChanged = new Subject();
  statusAuth: boolean;
  uid: string;

  @select(state => state.user) currentUser;

  isAdministrator: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isManager: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isOnlyManager: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: Http, private store: NgRedux<IAppState>) {
    this.currentUser.subscribe(user => {
      this.isAdministrator.next(user.role === USER_ROLE_ADMIN);
      this.isManager.next(user.role >= USER_ROLE_MANAGER);
      this.isOnlyManager.next(user.role === USER_ROLE_MANAGER);

      this.uid = user.id;
    });
  }

  register(properties): firebase.Promise<any> {
    let userId;

    return firebase.auth().createUserWithEmailAndPassword(properties.username, properties.password)
      .then(res => {
        userId = res.uid;
        return res.getToken();
      })
      .then(accessToken => {
        const user = Object.assign({}, properties, {id: userId});
        delete user.password;

        return createProfile(user, accessToken, this.http)
          .then(() => this.store.dispatch({type: UPDATE_USER, user: user}))
          .catch(() => this.store.dispatch({type: UPDATE_USER_FAIL}));
      });
  }

  login(properties: ILoginUser): firebase.Promise<any> {
    return firebase.auth().signInWithEmailAndPassword(properties.username, properties.password)
      .then(() => this.setStatusAuth(true));
  }

  logout() {
    return firebase.auth().signOut()
      .then(() => {
        this.store.dispatch({type: CLEAR_USERS});
        this.store.dispatch({type: CLEAR_ORDERS});

        this.setStatusAuth(false);
      });
  }

  getToken(): firebase.Promise<any> {
    const firebaseAuth = firebase.auth();
    if (!firebaseAuth.currentUser) {
      return Promise.reject('Need to sign in.');
    }

    return firebaseAuth.currentUser.getIdToken()
      .then((token: string) => this.token = token);
  }

  isAuthenticated(): boolean {
    return firebase.auth().currentUser !== null; // this.token !== null;
  }

  setStatusAuth(status: boolean): any {
    if (this.statusAuth === status) {
      return;
    }

    this.statusAuth = status;

    if (!this.statusAuth) {
      this.token = null;
      this.store.dispatch({type: LOGOUT_USER});
      this.onAuthChanged.next(this.statusAuth);
    } else {
      return this.getToken().then((token) => {
        this.token = token;
        this.onAuthChanged.next(this.statusAuth);
        const user = firebase.auth().currentUser;
        return fetchProfile(user.uid, this.token, this.http)
          .then(res => {
            if (!res) {
              throw new Error('Sorry, your user was removed.');
            }

            const profile = tassign(res, {username: user.email, id: user.uid});

            this.store.dispatch({
              type: INIT_USER,
              profile: profile
            });
          })
          .catch(err => {
            alert(err);
            this.logout();
            this.store.dispatch({type: INIT_USER_FAIL});
          });
      });
    }
  }

  setupAuthService(): void {
    const firebaseApp = firebase.initializeApp(config.firebase);

    firebaseApp['INTERNAL'].getToken()
      .then(res => {
        this.setStatusAuth(this.isAuthenticated());
      });
  }
}
