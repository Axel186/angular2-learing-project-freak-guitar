import {IUser, USER_ROLE_REGULAR} from '../user/model';
import {Http} from '@angular/http';
import {config} from '../../config';

const SERVER_URL = config.server.protocol + '://' + config.server.domain + '/';

export function createProfile(user: IUser, accessToken: string, http: Http): Promise<any> {
  user.role = USER_ROLE_REGULAR;
  return http.put(SERVER_URL + 'profiles/' + user.id + '.json?auth=' + accessToken, user)
    .toPromise()
    .then(response => response.json());
}

export function fetchProfile(userId: string, accessToken: string, http: Http): Promise<any> {
  return http.get(SERVER_URL + 'profiles/' + userId + '.json?auth=' + accessToken)
    .toPromise()
    .then(response => response.json());
}
