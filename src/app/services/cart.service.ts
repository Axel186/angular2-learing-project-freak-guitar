import {Injectable} from '@angular/core';
import {IProduct} from '../models/Product';
import {NgRedux} from 'ng2-redux';
import {ADD_ITEM_TO_CART, CLEAR_CART, DELETE_ITEM_FROM_CART, UPDATE_AMOUNT_OF_ITEM_IN_CART} from '../cart/actions';
import {IAppState} from '../store';
import {ICartState} from '../cart/model';

@Injectable()
export class CartService {

  constructor(private store: NgRedux<IAppState>) {
  }

  add(product: IProduct, amount: number) {
    this.store.dispatch({type: ADD_ITEM_TO_CART, productId: product.id, amount: amount});
  }

  updateAmount(product: IProduct, amount: number) {
    this.store.dispatch({type: UPDATE_AMOUNT_OF_ITEM_IN_CART, productId: product.id, amount: amount});
  }

  delete(product: IProduct) {
    this.store.dispatch({type: DELETE_ITEM_FROM_CART, productId: product.id});
  }

  clear() {
    this.store.dispatch({type: CLEAR_CART});
  }
}
