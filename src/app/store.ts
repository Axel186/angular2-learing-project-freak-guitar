import {combineReducers} from 'redux';
import {IProductsState, PRODUCTS_INITIAL_STATE} from './products/model';
import {productsReducer} from './products/store';
import {cartReducer} from './cart/store';
import {CART_INITIAL_STATE, ICartState} from './cart/model';
import {ordersReducer} from './orders/store';
import {IOrdersState, ORDERS_INITIAL_STATE} from './orders/model';
import {IUser, USER_INIT} from './user/model';
import {userReducer} from './user/store';
import {IUsersState, USERS_INITIAL_STATE} from './users/model';
import {usersReducer} from './users/store';

export interface IAppState {
  user: IUser;
  products: IProductsState;
  cart: ICartState;
  orders: IOrdersState;
  users: IUsersState;
}

export const INITIAL_STATE: IAppState = {
  // user: USER_INITIAL_STATE,
  user: USER_INIT,
  products: PRODUCTS_INITIAL_STATE,
  cart: CART_INITIAL_STATE,
  orders: ORDERS_INITIAL_STATE,
  users: USERS_INITIAL_STATE
};

export const rootReducer = combineReducers<IAppState>({
  user: userReducer,
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer,
  users: usersReducer
});
