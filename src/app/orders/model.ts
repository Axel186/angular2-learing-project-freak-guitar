import {ICartItem} from '../cart/model';

export const ORDER_STATUS_NEW = 'new';


export interface IOrder {
  id?: string;
  items: Array<ICartItem>;
  userId: string;
  status?: string;
}

export const ORDER_STATE: IOrder = {
  id: null,
  items: [],
  userId: null,
  status: ORDER_STATUS_NEW
};

export interface IOrdersState {
  orders: Array<IOrder>;
}

export const ORDERS_INITIAL_STATE: IOrdersState = {
  orders: []
};
