import {IOrder, IOrdersState, ORDERS_INITIAL_STATE} from './model';
import {
  CREATE_ORDER,
  createOrder,
  DELETE_ORDER,
  FETCH_ORDERS,
  fetchOrders,
  UPDATE_ORDER,
  updateOrder,
  deleteOrder, CLEAR_ORDERS
} from './actions';

export function ordersReducer(state: IOrdersState = ORDERS_INITIAL_STATE, action): IOrdersState {
  switch (action.type) {
    case FETCH_ORDERS:
      return fetchOrders(state, action);
    case CREATE_ORDER:
      return createOrder(state, action);
    case UPDATE_ORDER:
      return updateOrder(state, action);
    case DELETE_ORDER:
      return deleteOrder(state, action);
    case CLEAR_ORDERS:
      return ORDERS_INITIAL_STATE;
    default:
      return state;
  }
}
