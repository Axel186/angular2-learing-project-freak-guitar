import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgRedux, select} from 'ng2-redux';
import {OrdersService} from '../../../services/orders.service';
import {IAppState} from '../../../store';
import {IOrder} from '../../model';
import {DELETE_ORDER} from '../../actions';
import {AuthService} from '../../../services/auth.service';
import {IProduct} from '../../../models/Product';
import {IUser} from '../../../user/model';
import {Observable} from 'rxjs/Observable';
import {UsersService} from '../../../services/users.service';
import {ProductsService} from '../../../services/products.service';
import {FormAlert} from '../../../models/FormAlert';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.sass']
})
export class OrdersComponent implements OnInit, OnDestroy {
  alive = true;

  formAlert: FormAlert;

  @select(state => state.orders.orders) orders;
  @select(state => state.users) users;

  constructor(private ordersService: OrdersService, private store: NgRedux<IAppState>, private authService: AuthService, private usersService: UsersService, private productsService: ProductsService) {
    this.formAlert = new FormAlert();

    // Once you logged in
    this.store.select(state => state.user).takeWhile(() => this.alive).subscribe(user => {
      if (user.id) {
        this.ordersService.fetch();
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  cancelOrder(order: IOrder) {
    if (!confirm('Are you sure?')) {
      return;
    }

    this.ordersService.delete(order);
  }

  removeProduct(order: IOrder, product: any): void {
    if (!confirm('Are you sure?')) {
      return;
    }

    order.items = order.items.filter(item => item.productId !== product.productId);
  }

  updateOrder(order: IOrder) {
    this.formAlert.setLoading(true);

    this.ordersService.update(order)
      .then(() => {
        this.formAlert.success('Successfully updated!');
      })
      .catch(err => {
        this.formAlert.danger(err);
      });
  }
}
