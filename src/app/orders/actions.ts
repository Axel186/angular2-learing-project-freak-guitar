import {IOrder, IOrdersState} from './model';
import {tassign} from 'tassign';

export const FETCH_ORDERS = 'fetch_orders';
export const CREATE_ORDER = 'create_order';
export const UPDATE_ORDER = 'update_order';
export const DELETE_ORDER = 'delete_order';
export const CLEAR_ORDERS = 'clear_orders';
// export const CREATE_ORDER_FAIL = 'create_order_fail';


export function fetchOrders(state: IOrdersState, action: { orders: Array<IOrder> }): IOrdersState {
  const orders = state.orders.slice(0); // Clone

  Object.keys(action.orders).forEach(id => {
    const order = tassign(action.orders[id], {id: id});
    const found = orders.find(item => item.id === order.id);
    if (found) {
      const index = state.orders.map(item => item.id).indexOf(order.id);
      orders[index] = tassign(orders[index], order);
    } else {
      orders.push(order);
    }
  });

  return tassign(state, {orders: orders});
}

export function createOrder(state: IOrdersState, action: { order: IOrder }): IOrdersState {
  return tassign(state, {orders: state.orders.concat(action.order)});
}

export function updateOrder(state: IOrdersState, action: { order: IOrder }): IOrdersState {
  const order = state.orders.find(item => item.id === action.order.id);
  const index = state.orders.map(item => item.id).indexOf(action.order.id);

  return tassign(state, {
    orders: [
      ...state.orders.slice(0, index),
      tassign(order, action.order),
      ...state.orders.slice(index + 1)
    ]
  });
}

export function deleteOrder(state: IOrdersState, action: { orderId: string }): IOrdersState {
  return tassign(state, {orders: state.orders.filter(order => order.id !== action.orderId)});
}
