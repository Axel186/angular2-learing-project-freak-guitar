import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {select} from 'ng2-redux';

@Injectable()
export class AppRouteGuardService implements CanActivate {

  @select(state => state.user) currentUser;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.currentUser.map(user => {
      // is Signed in?
      if (user.id) {
        return true;
      }

      this.router.navigateByUrl('/');
      return false;
    });
  }
}
