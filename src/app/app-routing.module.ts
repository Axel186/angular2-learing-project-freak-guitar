import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {LoginPageComponent} from './user/login-page/login-page.component';
import {RegistrationPageComponent} from './user/registration-page/registration-page.component';
import {MyProfilePageComponent} from './user/my-profile-page/my-profile-page.component';
import {CategoryPageComponent} from './products/category-page/category-page.component';
import {CartPageComponent} from './cart/cart-page/cart-page.component';
import {OrdersPageComponent} from './orders/orders-page/orders-page.component';
import {AppRouteGuardService} from './appRouteGuard.service';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomePageComponent},
  {path: 'login', component: LoginPageComponent},
  {path: 'registration', component: RegistrationPageComponent},
  {path: 'my-profile', component: MyProfilePageComponent, canActivate: [AppRouteGuardService]},
  {path: 'category/:id', component: CategoryPageComponent},
  {path: 'cart', component: CartPageComponent},
  {path: 'orders', component: OrdersPageComponent, canActivate: [AppRouteGuardService]},

  // Lazy loading issues... Not works correctly - have to check it.
  // {path: 'manage-product', loadChildren: './products/manageProducts/manageProducts.module#ManageProductsModule'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {enableTracing: false})
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
