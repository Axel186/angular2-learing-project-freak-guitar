import {IUsersState, USERS_INITIAL_STATE} from './model';
import {
  CLEAR_USERS,
  DELETE_USER, deleteUser, FETCH_USERS, FETCH_USERS_SUCCESS, fetchUsers, fetchUsersSuccess, UPDATE_USERS,
  updateUsers
} from './actions';
import {INITIAL_STATE} from '../store';

export function usersReducer(state: IUsersState = USERS_INITIAL_STATE, action): IUsersState {
  switch (action.type) {
    case FETCH_USERS:
      return fetchUsers(state, action);
    case FETCH_USERS_SUCCESS:
      return fetchUsersSuccess(state, action);
    case DELETE_USER:
      return deleteUser(state, action);
    case UPDATE_USERS:
      return updateUsers(state, action);
    case CLEAR_USERS:
      return USERS_INITIAL_STATE;
    default:
      return state;
  }
}
