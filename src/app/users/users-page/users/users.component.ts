import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../../../services/users.service';
import {NgRedux, select} from 'ng2-redux';
import {IAppState} from '../../../store';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit, OnDestroy {
  alive = true;
  @select(state => state.users.users) users;

  constructor(private usersService: UsersService, private store: NgRedux<IAppState>, private authService: AuthService) {
    this.store.select(state => state.user).takeWhile(() => this.alive).subscribe(user => {
      if (user.id) { // Once user ready;
        this.usersService.fetchAll();
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
