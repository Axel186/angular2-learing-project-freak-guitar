import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersPageComponent} from './users-page/users-page.component';
import {UsersComponent} from './users-page/users/users.component';
import {UsersRoutingModule} from './users-routing.module';
import {SharedModule} from '../shared/shared.module';
import { UserPageComponent } from './user-page/user-page.component';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule
  ],
  declarations: [
    UsersPageComponent,
    UsersComponent,
    UserPageComponent,
  ]
})
export class ManageUsersModule {
}
