import {IUsersState, USERS_INITIAL_STATE} from './model';
import {IUser} from '../user/model';
import {tassign} from 'tassign';

export const FETCH_USERS = 'fetch_users';
export const FETCH_USERS_SUCCESS = 'fetch_users_success';
export const DELETE_USER = 'delete_user';
export const UPDATE_USERS = 'update_users';
export const CLEAR_USERS = 'clear_users';

export function fetchUsers(state: IUsersState, action): IUsersState {
  return tassign(state, {action: 'fetching'});
}

export function fetchUsersSuccess(state: IUsersState, action): IUsersState {
  const users = state.users.slice(0); // Clone
  const newUsers: Array<IUser> = [];

  Object.keys(action.users).forEach(id => {
    const user = tassign(action.users[id], {id: id});
    const found = users.find(item => item.id === user.id);

    if (found) {
      const index = state.users.map(item => item.id).indexOf(user.id);
      users[index] = tassign(users[index], user);
    } else {
      newUsers.push(user);
    }
  });

  if (state.action === 'fetching') {
    return {users: state.users.concat(newUsers), action: 'fetched'};
  } else {
    return tassign(state, {action: 'fetched'});
  }
}

export function deleteUser(state: IUsersState, action: { userId: string }): IUsersState {
  return tassign(state, {users: state.users.filter(user => user.id !== action.userId)});
}

export function updateUsers(state: IUsersState, action: { profile: Object, id: string }): IUsersState {
  const user = state.users.find(item => item.id === action.id);
  const index = state.users.map(item => item.id).indexOf(action.id);

  const users = state.users.slice(0); // Clone
  users[index] = tassign(users[index], action.profile);

  return tassign(state, {users: users, action: 'updated'});
}
