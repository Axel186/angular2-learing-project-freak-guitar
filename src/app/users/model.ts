import {IUser} from '../user/model';

export interface IUsersState {
  users: Array<IUser>;
  action: string;
}

export const USERS_INITIAL_STATE: IUsersState = {
  users: [],
  action: ''
};

// export const USERS_INITIAL_STATE: Array<IUser> = [];
