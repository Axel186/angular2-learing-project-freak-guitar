import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {select} from 'ng2-redux';
import {USER_ROLE_ADMIN} from '../user/model';


@Injectable()
export class ManageUsersGuardService implements CanActivate {

  @select(state => state.user) currentUser;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.currentUser.map(user => {
      if (user.role >= USER_ROLE_ADMIN) {
        return true;
      }

      this.router.navigateByUrl('/');
      return false;
    });
  }
}
