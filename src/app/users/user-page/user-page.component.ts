import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgRedux, select} from 'ng2-redux';
import {ActivatedRoute} from '@angular/router';
import {IAppState} from '../../store';
import {IUser} from '../../user/model';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.sass']
})
export class UserPageComponent implements OnInit, OnDestroy {
  alive = true;

  @select(state => state.users.users) users;

  userId: string;
  user: IUser;

  updateProfileRef;

  constructor(private route: ActivatedRoute, private usersService: UsersService, private authService: AuthService) {
    this.route.params.takeWhile(() => this.alive).subscribe(params => {
      this.userId = params.id;
    });

    // Find the user.
    this.users.takeWhile(() => this.alive).subscribe(users => {
      const user = users.find(item => item.id === this.userId);

      if (user) {
        this.user = user;
      } else {
        // Fetch if needed.

        // What until auth is ready.
        this.authService.onAuthChanged.takeWhile(() => this.alive).subscribe(status => {
          if (status && !this.user) {
            this.usersService.fetch(this.userId);
          }
        });
      }
    });

    const self = this;
    this.updateProfileRef = function (user) {
      return self.updateProfile(user);
    };
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.alive = false;
  }

  updateProfile(user) {
    return this.usersService.update(user);
  }
}
