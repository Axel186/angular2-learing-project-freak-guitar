import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UsersPageComponent} from './users-page/users-page.component';
import {UserPageComponent} from './user-page/user-page.component';
import {ManageUsersGuardService} from './manageUsersGuard.service';

const routes: Routes = [
  {path: 'users', component: UsersPageComponent, canActivate: [ManageUsersGuardService]},
  {path: 'user/:id', component: UserPageComponent, canActivate: [ManageUsersGuardService]},
  // {path: 'category/:id', component: CategoryPageComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class UsersRoutingModule {
}
