import {CART_INITIAL_STATE, ICartState} from './model';
import {
  ADD_ITEM_TO_CART, addItemToCart, CLEAR_CART, clearCart, DELETE_ITEM_FROM_CART, deleteItemFromCart,
  UPDATE_AMOUNT_OF_ITEM_IN_CART,
  updateAmountOfItemInCart
} from './actions';

export function cartReducer(state: ICartState = CART_INITIAL_STATE, action): ICartState {
  switch (action.type) {
    case ADD_ITEM_TO_CART:
      return addItemToCart(state, action);
    case UPDATE_AMOUNT_OF_ITEM_IN_CART:
      return updateAmountOfItemInCart(state, action);
    case DELETE_ITEM_FROM_CART:
      return deleteItemFromCart(state, action);
    case CLEAR_CART:
      return clearCart(state, action);
    default:
      return state;
  }
}
