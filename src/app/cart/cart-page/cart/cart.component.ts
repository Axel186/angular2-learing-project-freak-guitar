import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgRedux, select} from 'ng2-redux';
import {IProduct} from '../../../models/Product';
import {ICartItem} from '../../model';
import {IAppState} from '../../../store';
import {CartService} from '../../../services/cart.service';
import {OrdersService} from '../../../services/orders.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit, OnDestroy {
  alive = true;
  products: Array<IProduct>;
  itemsInCart: Array<ICartItem>;
  userId: string;

  @select(state => state.user) user;
  @select(state => state.products.products) productsRef;
  @select(state => state.cart.items) items;

  constructor(private store: NgRedux<IAppState>, private cartService: CartService, private ordersService: OrdersService, private router: Router) {
    this.items.takeWhile(() => this.alive).subscribe(items => this.itemsInCart = items);

    this.productsRef.takeWhile(() => this.alive).subscribe(products => {
      this.products = products.filter(product => this.idsInCart().indexOf(product.id) > -1);
    });

    this.user.takeWhile(() => this.alive).subscribe(user => this.userId = user.id);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  idsInCart(): Array<string> {
    return this.itemsInCart.map(item => item.productId);
  }

  getAmount(product: IProduct): number {
    const itemCart = this.itemsInCart.find(item => item.productId === product.id);
    return (itemCart) ? itemCart.amount : 0;
  }

  updateAmount(product: IProduct, value) {
    const item = this.itemsInCart.find(item => item.productId === product.id);
    item.amount = Number.parseInt(value);
  }

  calculateBill(): number {
    let totalPrice = 0;

    this.products.forEach(product => {
      const itemPrice = product.price * this.getAmount(product);
      if (itemPrice > 0) {
        totalPrice += product.price * this.getAmount(product);
      }
    });

    return totalPrice;
  }

  removeFromCart(product: IProduct): void {
    if (!confirm('Are you sure?')) {
      return;
    }

    this.cartService.delete(product);
    this.products = this.products.filter(item => item.id !== product.id);
  }

  clearAllCart(withoutConfirmation?: boolean): void {
    if (!withoutConfirmation && !confirm('Are you sure?')) {
      return;
    }

    this.cartService.clear();
    this.products = [];
  }

  completeOrder(): void {
    const uid: string = this.userId;

    if (!uid) {
      alert('You have to sign in');
      this.router.navigate(['/login']);
      return;
    }

    if (this.itemsInCart.length && uid) {
      this.ordersService.create({
        items: this.itemsInCart,
        userId: uid
      })
        .then((orderId: string) => {
          this.clearAllCart(true);
          this.router.navigate(['/orders']);
        })
        .catch(err => alert(err));
    } else {
      alert('Your cart is empty...');
    }
  }
}
