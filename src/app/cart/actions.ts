import {ICartItem, ICartState} from './model';
import {tassign} from 'tassign';

export const ADD_ITEM_TO_CART = 'add_item_to_cart';
export const UPDATE_AMOUNT_OF_ITEM_IN_CART = 'update_amount_of_item_in_cart';
export const DELETE_ITEM_FROM_CART = 'delete_item_from_cart';
export const CLEAR_CART = 'clear_cart';

export function addItemToCart(state: ICartState, action: { productId: string, amount: number }): ICartState {
  const itemExists = state.items.find(item => item.productId === action.productId);
  if (itemExists) {
    return state;
  }

  const item: ICartItem = {
    productId: action.productId,
    amount: action.amount
  };

  return tassign(state, {items: state.items.concat(item)});
}

export function updateAmountOfItemInCart(state: ICartState, action: { productId: string, amount: number }): ICartState {
  const item = state.items.find(item => item.productId === action.productId);
  item.amount = action.amount;
  const index = state.items.map(item => item.productId).indexOf(action.productId);

  return tassign(state, {
    items: [
      ...state.items.slice(0, index),
      item,
      ...state.items.slice(index + 1)
    ]
  });
}

export function deleteItemFromCart(state: ICartState, action: { productId: string }): ICartState {
  return tassign(state, {
    items: state.items.filter(item => item.productId !== action.productId)
  });
}

export function clearCart(state: ICartState, action: {}): ICartState {
  return tassign(state, {
    items: [] //state.items.splice(0)
  });
}
