export interface ICartItem {
  productId: string;
  amount: number;
}

export const CART_ITEM_INITIAL_STATE: ICartItem = {
  productId: null,
  amount: 0
};

export interface ICartState {
  items: Array<ICartItem>;
}

export const CART_INITIAL_STATE: ICartState = {
  items: []
};




