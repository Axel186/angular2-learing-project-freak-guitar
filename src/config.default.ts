export const config = {
  firebase: {
    apiKey: '',
    authDomain: ''
  },
  server: {
    domain: '',
    protocol: 'https'
  },
  recaptcha: {
    sitekey: ''
  }
};
